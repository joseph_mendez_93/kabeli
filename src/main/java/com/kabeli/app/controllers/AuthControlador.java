package com.kabeli.app.controllers;

import com.kabeli.app.dao.IUsuarioDao;
import com.kabeli.app.models.dto.LoginDto;
import com.kabeli.app.models.dto.RegistroDto;
import com.kabeli.app.models.entity.Usuario;
import com.kabeli.app.seguridad.JWTAuthResonseDTO;
import com.kabeli.app.seguridad.JwtTokenProvider;
import com.kabeli.app.services.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/auth")
public class AuthControlador {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUsuarioDao usuarioDao;

    @Autowired
    private IUsuarioService usuarioService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;


    @PostMapping("/iniciarSesion")
    public ResponseEntity<JWTAuthResonseDTO> authenticateUsuario(@RequestBody LoginDto loginDto){
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsuario(), loginDto.getContraseña()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        //Se obtiene el token
        String token = jwtTokenProvider.generarToken(authentication);

        return ResponseEntity.ok(new JWTAuthResonseDTO(token));
    }

    @PostMapping("/registrar")
    public ResponseEntity<?> registrarUsuario(@RequestBody RegistroDto registroDto) throws Exception {
        Map<String, Object> response = new HashMap<>();
        if (usuarioDao.existsByEmail(registroDto.getEmail())){
            response.put("Mensaje: ", "El correo ya existe");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        if(validarCorreo(registroDto.getEmail())){
            response.put("Mensaje: ", "El correo no cumple con el formato necesario");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        if(validarContraseña(registroDto.getPassword())){
            response.put("Mensaje: ", "La contraseña no cumple con el formato necesario");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        Usuario usuario = new Usuario();
        usuario.setNombre(registroDto.getName());
        usuario.setEmail(registroDto.getEmail());
        usuario.setContraseña(registroDto.getPassword());
        usuario.setTelefonos(registroDto.getPhones());

        usuarioService.save(usuario);
        response.put("Mensaje: Usuario registrado con exito. ", usuario);
        return new ResponseEntity<>( response, HttpStatus.OK);
    }

    private boolean validarCorreo(String correo){
        String expresionRegularEmail = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (correo != null) {
            if (Pattern.compile(expresionRegularEmail).matcher(correo).find()) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    private boolean validarContraseña(String contraseña){

        String expresionRegularContraseña = "^(?=(?:.*\\d){2})(?=(?:.*[A-Z]){1})(?=(?:.*[a-z]){1})\\S{8}$";
        if(contraseña != null){
            if (Pattern.compile(expresionRegularContraseña).matcher(contraseña).find()) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }
}
