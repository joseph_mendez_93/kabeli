package com.kabeli.app.models.entity;

import lombok.*;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Usuario")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String nombre;


    @NotNull(message = "El mail no puede ser null")
    private String email;

    @NotNull(message = "La contraseña no puede ser null")
    private String contraseña;

    @OneToMany
    private List<Phone> telefonos;


    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;


    @Temporal(TemporalType.DATE)
    private Date fechaUltActualizacion;


    @Temporal(TemporalType.DATE)
    private Date fechaUltConexion;


    private boolean estado;

}
