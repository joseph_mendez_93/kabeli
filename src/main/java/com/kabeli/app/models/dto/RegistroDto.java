package com.kabeli.app.models.dto;

import com.kabeli.app.models.entity.Phone;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@AllArgsConstructor
public class RegistroDto {
    private String name;
    private String email;
    private String password;
    private List<Phone> phones;
}
