package com.kabeli.app.services;

import com.kabeli.app.models.entity.Usuario;

import java.util.List;

public interface IUsuarioService {
    List<Usuario> findAll();
    Usuario save(Usuario usuario);

}
