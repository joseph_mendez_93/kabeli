package com.kabeli.app.services;

import com.kabeli.app.dao.IPhoneDao;
import com.kabeli.app.dao.IUsuarioDao;
import com.kabeli.app.models.entity.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

    @Autowired
    private IUsuarioDao usuarioDao;

    @Autowired
    private IPhoneDao phoneDao;

    @Override
    public List<Usuario> findAll() {
        return usuarioDao.findAll();
    }

    @Override
    public Usuario save(Usuario usuario) {
        //Se crea usuario y se guardan ambas entidades
        Date fechaHoy = new Date();
        usuario.setFechaCreacion(fechaHoy);
        usuario.setFechaUltActualizacion(fechaHoy);
        usuario.setFechaUltConexion(fechaHoy);
        usuario.setEstado(Boolean.TRUE);

        for (int i = 0; i < usuario.getTelefonos().size(); i++) {
            phoneDao.save(usuario.getTelefonos().get(i));
        }
        return usuarioDao.save(usuario);
    }

}