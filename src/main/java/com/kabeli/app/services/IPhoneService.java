package com.kabeli.app.services;

import com.kabeli.app.models.entity.Phone;

import java.util.List;

public interface IPhoneService {
    Phone save(Phone phone);
    List<Phone> findAll();

}
