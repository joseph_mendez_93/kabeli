package com.kabeli.app.dao;

import com.kabeli.app.models.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IUsuarioDao extends JpaRepository<Usuario, Long> {

    public Boolean existsByEmail(String email);

    public Optional<Usuario> findByUsernameOrEmail(String username,String email);
}
