package com.kabeli.app.dao;

import com.kabeli.app.models.entity.Phone;
import org.springframework.data.repository.CrudRepository;

public interface IPhoneDao extends CrudRepository<Phone, Long> {
}
